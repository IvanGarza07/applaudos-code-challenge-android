package com.applaudo.kitsu.detail

import android.text.TextUtils
import android.util.Log
import com.applaudo.kitsu.base.BasePresenter
import com.applaudo.kitsu.model.SerieResult

const val TAG = "DetailPresenter"

class DetailPresenter : BasePresenter<DetailContract.View>(), DetailContract.Presenter {


    override fun getSerie(link: String) {
        kitsuManager?.get(
                link,
                onSuccess = { result ->
                    val jsonResponse = result as String
                    if (!TextUtils.isEmpty(jsonResponse)) {
                        val serieResult = gSon.fromJson(jsonResponse, SerieResult::class.java)
                        view?.initSerie(serieResult.data)
                    } else
                        view?.showError("No data")
                },
                onError = { result ->
                    Log.e(TAG, "$result")
                    view?.showError(result as String)
                })
    }

    override fun getGenres(url: String) {

    }
}