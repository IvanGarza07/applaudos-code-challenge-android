package com.applaudo.kitsu.detail

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.applaudo.kitsu.R
import com.applaudo.kitsu.common.KitsuManager
import com.applaudo.kitsu.model.SerieObj
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*
import android.support.design.widget.AppBarLayout
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.applaudo.kitsu.player.PlayerActivity

class DetailActivity : AppCompatActivity(), DetailContract.View {

    private val detailPresenter = DetailPresenter()
    private var kitsuManager: KitsuManager? = null
    private var menu: Menu? = null
    private var showPlay = false
    private var title = ""
    private var videoId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val origin = intent.getStringExtra("origin")
        showPlay = TextUtils.equals(origin, "anime")

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)

        if (showPlay)
            fabYouTube.visibility = View.VISIBLE
        else
            fabYouTube.visibility = View.GONE

        app_bar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    showOption(R.id.actionShare)
                    if (showPlay)
                        showOption(R.id.actionPlay)
                } else if (isShow) {
                    isShow = false
                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                    hideOption(R.id.actionShare)
                    if (showPlay)
                        hideOption(R.id.actionPlay)
                }
            }
        })

        setUpMVP()
        detailPresenter.getSerie(intent.getStringExtra("url"))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu
        menuInflater.inflate(R.menu.menu_detail, menu)
        if (!showPlay)
            hideOption(R.id.actionPlay)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.actionShare -> {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, title)
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_label)))
            true
        }
        R.id.actionPlay -> {
            if (TextUtils.isEmpty(videoId))
                Toast.makeText(this, "Video not available", Toast.LENGTH_LONG).show()
            else
                startActivity(Intent(this, PlayerActivity::class.java)
                        .putExtra("video", videoId))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun initSerie(serie: SerieObj) {
        Glide.with(this)
                .load(serie.attributes.posterImage.medium)
                .into(imageViewPoster)
        textViewTitle.text = serie.attributes.title.en_jp
        textViewCanonical.text = serie.attributes.canonicalTitle
        title = serie.attributes.canonicalTitle
        textViewType.text = serie.attributes.showType
        textViewDate.text = resources.getString(R.string.date_value, serie.attributes.startDate, serie.attributes.endDate)
        textViewAverage.text = serie.attributes.averageRating
        if (serie.attributes.episodeLength > 0)
            textViewDuration.text = serie.attributes.episodeLength.toString()
        else
            textViewDuration.text = resources.getString(R.string.not_available_label)
        textViewAge.text = serie.attributes.ageRating
        textViewStatus.text = serie.attributes.status
        textViewSynopsis.text = serie.attributes.synopsis
        fabYouTube.setOnClickListener {
            videoId = serie.attributes.youtubeVideoId
            if (TextUtils.isEmpty(videoId))
                Toast.makeText(this, "Video not available", Toast.LENGTH_LONG).show()
            else
                startActivity(Intent(this, PlayerActivity::class.java)
                    .putExtra("video", videoId))
        }
    }

    override fun initGenres(genres: String) {

    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    private fun setUpMVP() {
        detailPresenter.attachView(this)
        kitsuManager = KitsuManager(this)
        kitsuManager?.setActivity(this)
        detailPresenter.initManager(kitsuManager)
    }

    private fun hideOption(id: Int) {
        val item = menu?.findItem(id)
        item?.isVisible = false
    }

    private fun showOption(id: Int) {
        val item = menu?.findItem(id)
        item?.isVisible = true
    }
}