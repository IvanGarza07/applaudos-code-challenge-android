package com.applaudo.kitsu.detail

import com.applaudo.kitsu.model.SerieObj

interface DetailContract {

    interface View {
        fun initSerie(serie: SerieObj)
        fun initGenres(genres: String)
        fun showError(error: String?)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter {
        fun getSerie(link: String)
        fun getGenres(url: String)
    }
}