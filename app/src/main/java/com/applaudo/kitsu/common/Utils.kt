package com.applaudo.kitsu.common

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

class Utils {
    companion object {

        fun isServerAvailable(): Boolean {
            val p1 = java.lang.Runtime.getRuntime().exec("ping -n 1 " + Constants.DOMAIN)
            val returnVal = p1.waitFor()
            return returnVal == 0
        }

        fun verifyConnection(context: Context?): Boolean {
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && isConnectedToWifiOrMobileData(activeNetwork)
        }

        private fun isConnectedToWifiOrMobileData(networkInfo: NetworkInfo): Boolean {
            return networkInfo.state == NetworkInfo.State.CONNECTED && (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE)
        }

        fun hideSoftKeyboard(activity: Activity, editText: EditText) {
            if (activity.currentFocus != null && activity.currentFocus is EditText) {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(editText.windowToken, 0)
            }
        }
    }
}