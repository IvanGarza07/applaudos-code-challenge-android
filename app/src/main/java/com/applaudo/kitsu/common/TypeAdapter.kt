package com.applaudo.kitsu.common

object TypeAdapter {
    const val CATEGORY = 1
    const val SEARCH = 2
}