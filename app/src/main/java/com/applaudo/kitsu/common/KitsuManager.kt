package com.applaudo.kitsu.common

import android.app.Activity
import android.content.Context
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit

class KitsuManager(private val context: Context?) {

    private var activity: Activity? = null
    private var okHttpClient: OkHttpClient? = null
    private var twitFeedPreference = KitsuPreference.newInstance(context, "kitsu_preferences")

    companion object {
        const val TOKEN = "token"
        const val LOCATION = "location"
    }

    init {
        okHttpClient = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
    }

    fun setActivity(activity: Activity?) {
        this.activity = activity
    }

    fun get(urlString: String, onSuccess: ((result: Any?) -> Unit)?, onError: ((result: Any?) -> Unit)?) {

        val request = Request.Builder()
                .url(urlString)
                .get()
                .build()

        okHttpClient
                ?.newCall(request)
                ?.enqueue(object : Callback {

                    override fun onFailure(call: Call, e: IOException) {
                        activity?.runOnUiThread {
                            onError?.invoke(e.message)
                        }
                    }

                    @Throws(IOException::class)
                    override fun onResponse(call: Call, response: Response) {
                        val result = response.body()?.string()

                        activity?.runOnUiThread {
                            if (response.code() == 200)
                                onSuccess?.invoke(result)
                            else
                                onError?.invoke("Lo sentimos no hay datos disponibles")
                        }
                    }
                })
    }
}