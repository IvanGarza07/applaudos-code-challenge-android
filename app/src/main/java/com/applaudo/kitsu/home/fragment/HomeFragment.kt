package com.applaudo.kitsu.home.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.applaudo.kitsu.R
import com.applaudo.kitsu.common.KitsuManager
import com.applaudo.kitsu.common.Utils
import com.applaudo.kitsu.detail.DetailActivity
import com.applaudo.kitsu.home.CategoryAdapter
import com.applaudo.kitsu.home.HomeContract
import com.applaudo.kitsu.home.HomePresenter
import com.applaudo.kitsu.model.Category
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

private const val ORIGIN = "origin"

class HomeFragment: Fragment(), HomeContract.View {

    private val homePresenter = HomePresenter()
    private var kitsuManager: KitsuManager? = null
    private var recyclerViewSeries: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var linearEmptyResult: LinearLayout? = null
    private var imageViewNoResult: ImageView? = null
    private var textViewNoResult: TextView? = null
    private var origin = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            origin = it.getString(ORIGIN)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerViewSeries = view.findViewById(R.id.recyclerViewSeries)
        progressBar = view.findViewById(R.id.progressBar)
        linearEmptyResult = view.findViewById(R.id.linearEmptyResult)
        setUpMVP()
        if (Utils.verifyConnection(context))
            homePresenter.getCategories(origin)
        else
            homePresenter.countLocalRegister(origin)
        return view
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    override fun initCategories(categoryList: List<Category>) {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerViewSeries?.layoutManager = linearLayoutManager
        recyclerViewSeries?.adapter = CategoryAdapter(homePresenter)
    }

    override fun showError(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showNoConnection() {
        linearEmptyResult?.visibility = View.VISIBLE
        imageViewNoResult?.setImageResource(R.drawable.no_connection)
        textViewNoResult?.text = resources.getString(R.string.whoops_label)
        progressBar?.visibility = View.GONE
        recyclerViewSeries?.visibility = View.GONE
    }

    override fun showEmptyResult() {
        linearEmptyResult?.visibility = View.VISIBLE
        imageViewNoResult?.setImageResource(R.drawable.no_result)
        textViewNoResult?.text = resources.getString(R.string.whoops_label2)
        progressBar?.visibility = View.GONE
        recyclerViewSeries?.visibility = View.GONE
    }

    override fun notifyDataSearchChange() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onLocationClicked(event: ClickEvent) {
        val intentDetail =  Intent(context, DetailActivity::class.java)
                .putExtra("url", event.url)
                .putExtra("origin", origin)
        if (recyclerViewSeries?.findViewHolderForAdapterPosition(event.positionCategory) != null) {
            val viewParent = recyclerViewSeries?.findViewHolderForAdapterPosition(event.positionCategory)?.itemView
            val recyclerViewItems = viewParent?.findViewById(R.id.recyclerViewItems) as RecyclerView
            val viewChild = recyclerViewItems.findViewHolderForAdapterPosition(event.position).itemView
            val imageViewPoster = viewChild.findViewById(R.id.imageViewPoster) as ImageView
            val options = ActivityOptions.makeSceneTransitionAnimation(activity, imageViewPoster, "poster_transition")
            startActivity(intentDetail, options.toBundle())
        } else
            startActivity(intentDetail)
    }

    companion object {
        @JvmStatic
        fun newInstance(origin: String) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(ORIGIN, origin)
            }
        }
    }

    private fun setUpMVP() {
        homePresenter.attachView(this)
        kitsuManager = KitsuManager(context)
        kitsuManager?.setActivity(activity)
        homePresenter.initManager(kitsuManager)
    }

    class ClickEvent(val positionCategory: Int, val position: Int, val url: String)
}
