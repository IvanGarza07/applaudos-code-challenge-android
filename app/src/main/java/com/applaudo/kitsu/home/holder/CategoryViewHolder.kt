package com.applaudo.kitsu.home.holder

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.applaudo.kitsu.R
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.home.fragment.HomeFragment
import com.applaudo.kitsu.model.Category
import com.applaudo.kitsu.model.SerieObj
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.category_item.view.*
import org.greenrobot.eventbus.EventBus

class CategoryViewHolder(itemView: View?, val context: Context) : BaseViewHolder(itemView) {

    override fun bindItem(item: Any, position: Int) {
        val category = item as? Category

        itemView.textViewTitle.text = category?.attributes?.title
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val partnerListAdapter = SerieListAdapter(position, category?.serieList!!)
        itemView.recyclerViewItems.layoutManager = layoutManager
        itemView.recyclerViewItems.adapter = partnerListAdapter
    }

    inner class SerieListAdapter(private val positionCategory: Int, private val serieList: List<SerieObj>) : RecyclerView.Adapter<SerieListAdapter.SerieViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerieViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.view_item, parent, false)

            return SerieViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: SerieViewHolder, position: Int) {
            val serie = serieList[position]
            holder.textViewTitle?.text = serie.attributes.canonicalTitle
            Glide.with(context)
                    .load(serie.attributes.posterImage.medium)
                    .into(holder.imageViewPoster!!)
            holder.itemContainer?.setOnClickListener(ItemClickListener(positionCategory, position, serie.links.self))
        }

        override fun getItemCount(): Int {
            return serieList.size
        }

        inner class SerieViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
            var itemContainer: ConstraintLayout? = null
            var imageViewPoster: ImageView? = null
            var textViewTitle: TextView? = null

            init {
                itemContainer = view.findViewById(R.id.itemContainer)
                imageViewPoster = view.findViewById(R.id.imageViewPoster)
                textViewTitle = view.findViewById(R.id.textViewTitle)
            }
        }
    }

    class ItemClickListener(private val positionCategory: Int, private val position: Int, val link: String): View.OnClickListener {
        override fun onClick(v: View?) {
            EventBus.getDefault().post(HomeFragment.ClickEvent(positionCategory, position, link))
        }
    }

}