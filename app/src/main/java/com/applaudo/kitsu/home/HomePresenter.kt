package com.applaudo.kitsu.home

import android.annotation.SuppressLint
import android.text.TextUtils
import android.util.Log
import android.view.ViewGroup
import android.widget.Filter
import com.applaudo.kitsu.KitsuApplication
import com.applaudo.kitsu.base.BasePresenter
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.common.Constants
import com.applaudo.kitsu.model.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus

const val TAG = "HomePresenter"

class HomePresenter : BasePresenter<HomeContract.View>(), HomeContract.Presenter {

    private val categoryList: MutableList<Category> = arrayListOf()
    private val searchList: MutableList<SerieObj> = arrayListOf()
    private val searchListCopy: MutableList<SerieObj> = arrayListOf()
    private var searchFilter: SearchFilter? = null
    private var index = 0

    override fun getCategories(origin: String) {
        view?.showLoading()
        val url = Constants.DOMAIN + "categories"
        kitsuManager?.get(
                url,
                onSuccess = { result ->
                    val jsonResponse = result as String
                    if (!TextUtils.isEmpty(jsonResponse)) {
                        val categoriesResult = gSon.fromJson(jsonResponse, CategoriesDataResult::class.java)
                        getSeries(origin, categoriesResult.data)
                    } else {
                        view?.showError("No data")
                        view?.showNoConnection()
                    }
                },
                onError = { result ->
                    Log.e(TAG, "$result")
                    view?.showError(result as String)
                    view?.showEmptyResult()
                })
    }

    private fun getSeries(origin: String, categoryList: List<Category>) {
        val categoryId = categoryList[index].id
        val url = Constants.DOMAIN + "categories/$categoryId/$origin"
        kitsuManager?.get(
                url,
                onSuccess = { result ->
                    val jsonResponse = result as String
                    if (!TextUtils.isEmpty(jsonResponse)) {
                        val seriesResult = gSon.fromJson(jsonResponse, SerieDataResult::class.java)
                        insertSources(origin, categoryList[index].id, seriesResult.data)
                        categoryList[index].initSerieList(seriesResult.data)
                        EventBus.getDefault().post(HomeActivity.SearchListEvent(seriesResult.data))
                        this.categoryList.add(categoryList[index])
                        if (index == categoryList.size - 1) {
                            view?.hideLoading()
                            view?.initCategories(this.categoryList)
                        } else {
                            index++
                            getSeries(origin, categoryList)
                        }
                    } else
                        view?.showError("No data")
                },
                onError = { result ->
                    Log.e(TAG, "$result")
                    view?.showError(result as String)
                })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return BaseViewHolder.createViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int, adapter: Int) {
        when(adapter) {
            1 -> holder.bindItem(getCategoryRow(position), position)
            else -> holder.bindItem(getSearchRow(position), position)
        }
    }

    override fun getCategoryRow(position: Int): Any {
        return categoryList[position]
    }

    override fun getCategoryCount(): Int {
        return categoryList.size
    }

    override fun getSearchRow(position: Int): Any {
        return searchList[position]
    }

    override fun getSearchCount(): Int {
        return searchList.size
    }

    override fun addItemList(itemList: List<SerieObj>) {
        searchList.addAll(itemList)
        searchListCopy.addAll(itemList)
        view?.notifyDataSearchChange()
    }

    override fun getFilter(): Filter {
        if (searchFilter == null)
            searchFilter = SearchFilter(this, searchListCopy)
        return searchFilter as SearchFilter
    }

    override fun updateList(searchListFilter: List<SerieObj>) {
        searchList.clear()
        searchList.addAll(searchListFilter)
        view?.notifyDataSearchChange()
    }

    @SuppressLint("CheckResult")
    private fun insertSources(origin: String, category: String, serieList: List<SerieObj>) {
        if (!serieList.isEmpty()) {
            val serieListFinal: MutableList<Serie> = arrayListOf()
            for (serieObj: SerieObj in serieList) {
                serieListFinal.add(Serie(
                        serieObj.id,
                        category,
                        origin,
                        serieObj.type,
                        serieObj.links.self,
                        serieObj.attributes.title.en_jp,
                        serieObj.attributes.canonicalTitle,
                        serieObj.attributes.averageRating,
                        serieObj.attributes.startDate,
                        serieObj.attributes.endDate,
                        serieObj.attributes.ageRating,
                        serieObj.attributes.episodeCount,
                        serieObj.attributes.episodeLength,
                        serieObj.attributes.youtubeVideoId,
                        serieObj.attributes.showType,
                        serieObj.attributes.status,
                        serieObj.attributes.synopsis,
                        serieObj.attributes.posterImage.medium))
            }
            Single
                    .fromCallable { KitsuApplication.database?.serieDao()?.insertAll(serieListFinal) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {})
        }
    }

    @SuppressLint("CheckResult")
    override fun countLocalRegister(origin: String) {
        Single
                .fromCallable { KitsuApplication.database?.serieDao()?.getCount(origin) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            if (it!! > 0) {
                                //TODO show local resource
                            } else
                                view?.showEmptyResult()
                        },
                        { e ->
                            view?.showError(e.message)
                            view?.showEmptyResult()
                        })
    }

}