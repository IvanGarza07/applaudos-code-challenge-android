package com.applaudo.kitsu.home

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.model.SerieObj

class SearchAdapter(private val presenter: HomePresenter) : RecyclerView.Adapter<BaseViewHolder>(), Filterable {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return presenter.onCreateViewHolder(parent, viewType)
    }

    override fun getItemCount(): Int {
        return presenter.getSearchCount()
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        return presenter.onBindViewHolder(holder, position, 2)
    }

    override fun getItemViewType(position: Int): Int {
        val serie = presenter.getSearchRow(position) as SerieObj
        return serie.viewType
    }

    override fun getFilter(): Filter {
        return presenter.getFilter()
    }
}