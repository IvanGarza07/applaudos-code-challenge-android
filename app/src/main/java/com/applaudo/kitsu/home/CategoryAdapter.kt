package com.applaudo.kitsu.home

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.model.Category

class CategoryAdapter(private val presenter: HomePresenter) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return presenter.onCreateViewHolder(parent, viewType)
    }

    override fun getItemCount(): Int {
        return presenter.getCategoryCount()
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        return presenter.onBindViewHolder(holder, position, 1)
    }

    override fun getItemViewType(position: Int): Int {
        val category = presenter.getCategoryRow(position) as Category
        return category.viewType
    }

}