package com.applaudo.kitsu.home

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.applaudo.kitsu.R
import com.applaudo.kitsu.common.KitsuManager
import com.applaudo.kitsu.model.Category
import com.applaudo.kitsu.model.SerieObj
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.panel_search.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.applaudo.kitsu.common.TypeAdapter
import com.applaudo.kitsu.common.Utils

class HomeActivity : AppCompatActivity(), HomeContract.View {

    private val homePresenter = HomePresenter()
    private var kitsuManager: KitsuManager? = null
    private var searchAdapter: SearchAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.title = "Kitsu"
        setUpMVP()

        viewPagerHome.adapter = HomePagerAdapter(supportFragmentManager)
        viewPagerHome.offscreenPageLimit = 2
        smartTabHome.setViewPager(viewPagerHome)

        imageButtonBack.setOnClickListener {
            slidingLayoutHome?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        initEditTextSearch()
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.actionSearch -> {
            slidingLayoutHome?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun initCategories(categoryList: List<Category>) {

    }

    override fun notifyDataSearchChange() {
        recyclerViewItems.layoutManager = GridLayoutManager(this, 3)
        recyclerViewItems.adapter = SearchAdapter(homePresenter)
        searchAdapter?.notifyDataSetChanged()
    }

    override fun showError(error: String?) {

    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showNoConnection() {

    }

    override fun showEmptyResult() {

    }

    override fun onBackPressed() {
        if (slidingLayoutHome.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            Utils.hideSoftKeyboard(this, editTextSearch)
            val handler = Handler()
            handler.postDelayed({ slidingLayoutHome?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }, 150)
        } else
            super.onBackPressed()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun addItemList(event: SearchListEvent) {
        val searchList: MutableList<SerieObj> = arrayListOf()
        for (serieObj: SerieObj in event.itemList) {
            serieObj.viewType = TypeAdapter.SEARCH
            searchList.add(serieObj)
        }
        homePresenter.addItemList(searchList)
    }

    private fun setUpMVP() {
        homePresenter.attachView(this)
        kitsuManager = KitsuManager(this)
        kitsuManager?.setActivity(this)
        homePresenter.initManager(kitsuManager)
        searchAdapter = SearchAdapter(homePresenter)
    }

    private fun initEditTextSearch() {
        editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                searchAdapter?.filter?.filter(s.toString())
            }
        })
    }

    class SearchListEvent(val itemList: List<SerieObj>)
}
