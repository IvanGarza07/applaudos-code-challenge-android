package com.applaudo.kitsu.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.applaudo.kitsu.home.fragment.HomeFragment

class HomePagerAdapter(private val fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private val sectionsNameList = arrayOf("ANIME", "MANGA")

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> HomeFragment.newInstance("anime")
            else -> HomeFragment.newInstance("manga")
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return sectionsNameList[position]
    }

}