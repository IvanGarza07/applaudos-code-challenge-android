package com.applaudo.kitsu.home

import android.widget.Filter
import com.applaudo.kitsu.model.SerieObj

class SearchFilter(private val presenter: HomePresenter, private val searchList: MutableList<SerieObj>) : Filter() {

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        val constraintLow = constraint.toString().toLowerCase()
        val result = Filter.FilterResults()
        if (constraintLow.isNotEmpty()) {
            val filteredItems : MutableList<SerieObj> = arrayListOf()
            var i = 0
            val l = searchList.size
            while (i < l) {
                val searchItem = searchList[i]
                if (searchItem.attributes.canonicalTitle.toLowerCase().contains(constraintLow))
                    filteredItems.add(searchItem)
                i++
            }
            result.count = filteredItems.size
            result.values = filteredItems
        } else {
            synchronized(this) {
                result.values = searchList
                result.count = searchList.size
            }
        }
        return result
    }

    @Suppress("UNCHECKED_CAST")
    override fun publishResults(constraint: CharSequence?, result: FilterResults?) {
        presenter.updateList(result?.values as List<SerieObj>)
    }
}