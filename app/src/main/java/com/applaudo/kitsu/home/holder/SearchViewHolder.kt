package com.applaudo.kitsu.home.holder

import android.content.Context
import android.view.View
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.home.fragment.HomeFragment
import com.applaudo.kitsu.model.SerieObj
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_item.view.*
import org.greenrobot.eventbus.EventBus

class SearchViewHolder(itemView: View?, val context: Context) : BaseViewHolder(itemView) {

    override fun bindItem(item: Any, position: Int) {
        val serie = item as? SerieObj

        itemView.textViewTitle?.text = serie?.attributes?.canonicalTitle
        Glide.with(context)
                .load(serie?.attributes?.posterImage?.medium)
                .into(itemView.imageViewPoster!!)
        //itemView.itemContainer?.setOnClickListener(ItemClickListener(position, serie?.links?.self))
    }

    class ItemClickListener(private val positionCategory: Int, private val position: Int, val link: String): View.OnClickListener {
        override fun onClick(v: View?) {
            EventBus.getDefault().post(HomeFragment.ClickEvent(positionCategory, position, link))
        }
    }

}