package com.applaudo.kitsu.home

import android.view.ViewGroup
import android.widget.Filter
import com.applaudo.kitsu.base.BaseViewHolder
import com.applaudo.kitsu.model.Category
import com.applaudo.kitsu.model.SerieObj

interface HomeContract {

    interface View {
        fun initCategories(categoryList: List<Category>)
        fun notifyDataSearchChange()
        fun showError(error: String?)
        fun showLoading()
        fun hideLoading()
        fun showNoConnection()
        fun showEmptyResult()
    }

    interface Presenter {
        fun getCategories(origin: String)
        fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder
        fun onBindViewHolder(holder: BaseViewHolder, position: Int, adapter: Int)
        fun getCategoryRow(position: Int): Any
        fun getCategoryCount(): Int
        fun getSearchRow(position: Int): Any
        fun getSearchCount(): Int
        fun countLocalRegister(origin: String)
        fun addItemList(itemList: List<SerieObj>)
        fun getFilter(): Filter
        fun updateList(searchListFilter: List<SerieObj>)
    }
}