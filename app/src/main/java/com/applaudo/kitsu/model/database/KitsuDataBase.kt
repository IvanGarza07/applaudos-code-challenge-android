package com.applaudo.kitsu.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.applaudo.kitsu.model.Serie
import com.applaudo.kitsu.model.dao.SerieDao

@Database(entities = [(Serie::class)], version = 1, exportSchema = false)
abstract class KitsuDataBase : RoomDatabase() {
    abstract fun serieDao(): SerieDao
}