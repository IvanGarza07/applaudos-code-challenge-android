package com.applaudo.kitsu.model.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import android.arch.persistence.room.Update
import com.applaudo.kitsu.model.Serie

@Dao
abstract class SerieDao: BaseDao<Serie>() {

    @Query("SELECT count(*) FROM Serie WHERE tab = :tab")
    abstract fun getCount(tab: String): Long

    @Query("SELECT * FROM Serie WHERE tab = :tab ORDER BY id ASC")
    abstract fun getAll(tab: String): Flowable<List<Serie>>

    @Query("SELECT * FROM Serie WHERE id = :id")
    abstract fun getSerie(id: String): Flowable<List<Serie>>

    @Query("SELECT * FROM Serie WHERE id = :id")
    abstract fun findById(id: Long): Single<Serie>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(items: List<Serie>)

    @Update
    abstract fun updateSource(serie: Serie)

    @Query("DELETE FROM Serie WHERE tab = :tab")
    abstract fun deleteAll(tab: String)

}