package com.applaudo.kitsu.model

import com.applaudo.kitsu.common.TypeAdapter
import com.google.gson.annotations.SerializedName

class Category(
        @SerializedName("id") val id: String,
        @SerializedName("type") val type: String,
        @SerializedName("attributes") val attributes: Attribute,
        @SerializedName("relationships") val relationships: Relationship) {

    var viewType: Int = TypeAdapter.CATEGORY
    var serieList: List<SerieObj>? = null

    fun initSerieList(serieList: List<SerieObj>) {
        this.serieList = serieList
    }

    class Attribute(@SerializedName("title") val title: String, @SerializedName("description") val description: String)

    class Relationship(@SerializedName("anime") val anime: Relation, @SerializedName("manga") val manga: Relation)

    class Relation(@SerializedName("links") val links: Link)

    class Link(@SerializedName("self") val self: String, @SerializedName("related") val related: String)
}