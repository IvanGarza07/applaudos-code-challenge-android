package com.applaudo.kitsu.model

import com.applaudo.kitsu.common.TypeAdapter
import com.google.gson.annotations.SerializedName

class SerieObj(
        @SerializedName("id") val id: String,
        @SerializedName("type") val type: String,
        @SerializedName("links") val links: Link,
        @SerializedName("attributes") val attributes: Attribute,
        @SerializedName("relationships") val relationships: Relationship
) {

    var viewType: Int = TypeAdapter.SEARCH

    class Attribute(
            @SerializedName("titles") val title: Title,
            @SerializedName("canonicalTitle") val canonicalTitle: String,
            @SerializedName("synopsis") val synopsis: String,
            @SerializedName("averageRating") val averageRating: String,
            @SerializedName("startDate") val startDate: String,
            @SerializedName("endDate") val endDate: String,
            @SerializedName("ageRating") val ageRating: String,
            @SerializedName("status") val status: String,
            @SerializedName("youtubeVideoId") val youtubeVideoId: String,
            @SerializedName("showType") val showType: String,
            @SerializedName("episodeCount") val episodeCount: Int,
            @SerializedName("episodeLength") val episodeLength: Int,
            @SerializedName("posterImage") val posterImage: Image
    )

    class Title(@SerializedName("en_jp") val en_jp: String)

    class Image(@SerializedName("medium") val medium: String)

    class Relationship(@SerializedName("genres") val genres: Relation)

    class Relation(@SerializedName("links") val links: Link)

    class Link(@SerializedName("self") val self: String, @SerializedName("related") val related: String)
}