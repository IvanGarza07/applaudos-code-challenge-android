package com.applaudo.kitsu.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Serie(
        @PrimaryKey(autoGenerate = false) val id: String,
        val category: String?,
        val tab: String?,
        val type: String?,
        val link: String?,
        val title: String?,
        val canonicalTitle: String?,
        val averageRating: String?,
        val startDate: String?,
        val endDate: String?,
        val ageRating: String?,
        val episodeCount: Int?,
        val episodeLength: Int?,
        val youtubeVideoId: String?,
        val showType: String?,
        val status: String?,
        val synopsis: String?,
        val poster: String?
)