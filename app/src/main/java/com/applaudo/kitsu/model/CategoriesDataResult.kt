package com.applaudo.kitsu.model

import com.google.gson.annotations.SerializedName

class CategoriesDataResult(@SerializedName("data") val data: List<Category>)