package com.applaudo.kitsu.model

import com.google.gson.annotations.SerializedName

class SerieDataResult(@SerializedName("data") val data: List<SerieObj>)