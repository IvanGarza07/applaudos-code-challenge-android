package com.applaudo.kitsu.model

import com.google.gson.annotations.SerializedName

class SerieResult(@SerializedName("data") val data: SerieObj)