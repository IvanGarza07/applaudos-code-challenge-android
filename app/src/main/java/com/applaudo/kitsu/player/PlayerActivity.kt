package com.applaudo.kitsu.player

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.applaudo.kitsu.R
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity: YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {

    private var videoId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        videoId = intent.getStringExtra("video")
        youTubePlayerView.initialize("AIzaSyBzPg7gkO5ySsuPHdmWgHfzWHrahAvg2Zs", this)
    }

    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, player: YouTubePlayer?, wasRestored: Boolean) {
        if (!wasRestored) {
            player?.cueVideo(videoId)
        }
    }

    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show()
    }
}