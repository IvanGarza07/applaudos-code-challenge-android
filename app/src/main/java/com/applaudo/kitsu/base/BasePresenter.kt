package com.applaudo.kitsu.base

import com.applaudo.kitsu.common.KitsuManager
import com.google.gson.GsonBuilder

abstract class BasePresenter<V> {

    protected var view: V? = null
    protected var kitsuManager: KitsuManager? = null
    protected var gSon = GsonBuilder().setPrettyPrinting().create()

    open fun initManager(kitsuManager: KitsuManager?) {
        this.kitsuManager = kitsuManager
    }

    open fun attachView(view: V) {
        this.view = view
    }

    open fun detachView() {
        view = null
    }


    open fun cancelTask() {

    }

    protected val isViewAttached: Boolean
        get() = view != null

}