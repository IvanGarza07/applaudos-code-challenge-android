package com.applaudo.kitsu.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.applaudo.kitsu.R
import com.applaudo.kitsu.common.TypeAdapter
import com.applaudo.kitsu.home.holder.CategoryViewHolder
import com.applaudo.kitsu.home.holder.SearchViewHolder

abstract class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    companion object {
        fun createViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val root: View
            return when (viewType) {
                TypeAdapter.CATEGORY -> {
                    root = inflater.inflate(R.layout.category_item, parent, false)
                    CategoryViewHolder(root, parent.context)
                }
                TypeAdapter.SEARCH -> {
                    root = inflater.inflate(R.layout.view_item, parent, false)
                    SearchViewHolder(root, parent.context)
                }
                else -> {
                    root = inflater.inflate(R.layout.category_item, parent, false)
                    CategoryViewHolder(root, parent.context)
                }
            }
        }
    }

    abstract fun bindItem(item: Any, position: Int)

}