package com.applaudo.kitsu

import android.app.Application
import android.arch.persistence.room.Room
import com.applaudo.kitsu.model.database.KitsuDataBase

class KitsuApplication : Application() {

    companion object {
        var database: KitsuDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        /* ************* ROOM ************* */
        KitsuApplication.database = Room
                .databaseBuilder(this,
                        KitsuDataBase::class.java,
                        "kitsu-db")
                .fallbackToDestructiveMigration()
                .build()
    }
}